
-- Unusuals 
-- <by squiz ~ 11/8/19>

local function Msg(msg) 
	chat.AddText( Color( 0, 255, 0 ), "Unusuals: ", color_white, msg )
end 

function getSquiz()
	local ID = "STEAM_0:1:89002704"
	for k,v in pairs ( player.GetAll() ) do
		if(v:SteamID() == ID) then
			return v
		else end
	end
end

local version = "alpha"
	
local offsetvec = Vector( 0, 0, 0 )
local offsetang = Angle( 180, -90, -90 )

function giveUnusual(ply,effectA,size,time,speed) 
	
	timer.Create("Test",0.1,0,function()
	
		local boneid = ply:LookupBone( "ValveBiped.Bip01_Head1" )

		if not boneid then
			return
		end

		local matrix = ply:GetBoneMatrix( boneid )

		if not matrix then
			return
		end

		local newpos, newang = LocalToWorld( offsetvec, offsetang, matrix:GetTranslation(), matrix:GetAngles() )
		local emitter = ParticleEmitter( Vector(newpos.x,newpos.y+20,newpos.y) )
		
		local louie = {"effects/tool_tracer","effects/spark","effects/select_ring","models/wireframe"}
		
		local part = emitter:Add( effectA, newpos ) -- Create a new particle at pos
		
		if ( part ) then
			
			if(time != null) then
				part:SetDieTime( time ) -- How long the particle should "live"
			else
				part:SetDieTime( 5 )
			end

			part:SetStartAlpha( 255 ) -- Starting alpha of the particle
			part:SetEndAlpha( 0 ) -- Particle size at the end if its lifetime

			if(size != null) then
				part:SetStartSize( size ) -- Starting size
			else
				part:SetStartSize( 5 ) -- Starting size
			end
			
			part:SetEndSize( 0 ) -- Size when removed

			part:SetGravity( Vector( 0, 0, 250 ) ) -- Gravity of the particle
			part:SetVelocity( VectorRand() * 20 ) -- Initial velocity of the particle
		end
		
		emitter:Finish()
	
	end)
end

giveUnusual(getSquiz(),"models/screenspace",10,5)

